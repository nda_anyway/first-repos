import axios from 'axios'
import database from './database.mjs'

async function fetchRates (base = 'USD') {
	const RATES_API_BASE_URL = 'https://v6.exchangerate-api.com/v6'
	const RATES_API_KEY = '258c9dfd2384dc80de947d74'

	try {
		const response = await axios.get(`${RATES_API_BASE_URL}/${RATES_API_KEY}/latest/${base}`)

		return response.data
	} catch (e) {
		console.error(e)
		return null
	}
}

async function getLatestSavedRates (base = 'USD') {
	const rates = await database.db().collection('rates').find({ base }).sort({ createdAt: -1 }).limit(1).toArray()
	return rates[0]
}

async function saveRates (rates, base) {
	if (!rates && !rates['USD'])
		throw 'Cannot save rates: given object is invalid'

	const createdAt = new Date().getTime()

	try {
		await database.db().collection('rates').insertOne({
			base,
			createdAt,
			rates
		})

		return true
	} catch (e) {
		return false
	}
}

export default {
	fetchRates, saveRates, getLatestSavedRates
} 


