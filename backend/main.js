import express from 'express'

import constants from './constants.mjs'
import functions from './functions.mjs'
import mongo from './database.mjs'

const app = express()

app.get('/rates', async (request, response) => {
	const base = request.query.from
	const now = new Date().getTime()
	const latestSavedRates = await functions.getLatestSavedRates(base)

	if (latestSavedRates && (latestSavedRates.createdAt - now < constants.time.FIVE_MINITES))
		return response.json(latestSavedRates)
	else {
		const rates = await functions.fetchRates(base)
		const success = await functions.saveRates(rates.conversion_rates, rates.base_code)
	
		if (success)
			return response.json(await functions.getLatestSavedRates(base))
		else 
			return response.json(null)
	}
})

app.get('/currencies', async (request, response) => {
	return response.json(constants.supported.CURRENCIES)
})

async function main () {
	await mongo.connect()

	const rates = await mongo.db().collection('rates').find().toArray()
	
	process.stdout.write('\n\n')
	console.log(rates)
	process.stdout.write('Backend is running on localhost:3030')

	await app.listen(3030, '0.0.0.0')
}

main()